﻿#あそぼうよ！
#####Version - Not yet released - 8/22/15
An Android application for studying Japanese by listening to songs and filling in the missing lyrics.

##Environment Settings
JDK version: 1.8.0_31

Android SDK version: API 19 Android 4.4 (KitKat) -> API 22 Android 5.1 (Lollipop)

Build Tools version: 23.0.0 rc2

Gradle version: 2.2.1

Android Plugin Version: 1.2.3

Library Repository: jcenter, mavenCentral, 'https://maven.fabric.io/public'

##Project Description
A user will log in using Twitter Digits and then proceed to select from a list of songs which are categorized as 'Slow', 'Medium', and 'Fast'. After selecting a song, the user will be prompted to select a mode in which to play the game. The first and easier mode is called 'TapMode' due to lack of creative minds on the team. Answers will appear on the screen as the song progresses and the user will choose from among them to gain points. The alternative game mode, 'TypeMode', is more difficult, requiring the user to directly type in the answer from the keyboard and not providing any hints. The portion of the lyrics that is missing in each line is determined at random each time the song is played, so there is no competitive aspect between other players. This application is simply meant for help with self-study and enjoying your own songs.

##Song Requests
If there are songs that would like to be seen on the application, requests can be sent to the developers emails below.

##Contact Us
Reline - nathanielreline@gmail.com

benbaido - mor3thanb3for3@gmail.com
